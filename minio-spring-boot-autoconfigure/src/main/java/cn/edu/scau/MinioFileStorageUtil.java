package cn.edu.scau;


import io.minio.GetObjectArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@EnableConfigurationProperties(MinioConfigurationProperties.class)
@Import(MinioAutoConfiguration.class)
public class MinioFileStorageUtil {

    private final MinioClient minioClient;

    private final MinioConfigurationProperties minioConfigurationProperties;

    public MinioFileStorageUtil(MinioClient minioClient, MinioConfigurationProperties minioConfigurationProperties) {
        this.minioClient = minioClient;
        this.minioConfigurationProperties = minioConfigurationProperties;
    }

    private final static String separator = "/";

    /**
     * @param path     文件路径
     * @param filename yyyy/MM/dd/filename.jpg
     * @return String
     */
    public String buildFilePath(String path, String filename) {
        StringBuilder stringBuilder = new StringBuilder(50);
        if (path != null && !path.isEmpty()) {
            stringBuilder.append(path).append(separator);
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String todayString = simpleDateFormat.format(new Date());
        stringBuilder.append(todayString)
                .append(separator)
                .append(filename);
        return stringBuilder.toString();
    }

    /**
     * 上传图片文件-jpg
     *
     * @param prefix      文件前缀
     * @param filename    文件名
     * @param inputStream 文件流
     * @return 文件全路径
     */
    public String uploadImageFile(String prefix, String filename, InputStream inputStream) {
        String filePath = buildFilePath(prefix, filename);
        try {
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .object(filePath)
                    .contentType("image/jpg")
                    .bucket(minioConfigurationProperties.getBucket()).stream(inputStream, inputStream.available(), -1)
                    .build();
            minioClient.putObject(putObjectArgs);

            return minioConfigurationProperties.getEndpoint() + separator + minioConfigurationProperties.getBucket() + separator + filePath;
        } catch (Exception e) {
            log.error("Minio put file error.", e);
            throw new RuntimeException("Minio put file error.");
        }
    }

    /**
     * 上传html文件
     *
     * @param prefix      文件前缀
     * @param filename    文件名
     * @param inputStream 文件流
     * @return 文件全路径
     */
    public String uploadHtmlFile(String prefix, String filename, InputStream inputStream) {
        String filePath = buildFilePath(prefix, filename);
        try {
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .object(filePath)
                    .contentType("text/html")
                    .bucket(minioConfigurationProperties.getBucket()).stream(inputStream, inputStream.available(), -1)
                    .build();
            minioClient.putObject(putObjectArgs);
            return minioConfigurationProperties.getEndpoint() + separator + minioConfigurationProperties.getBucket() + separator + filePath;
        } catch (Exception e) {
            log.error("Minio put file error.", e);
            throw new RuntimeException("Minio put file error.");
        }
    }

    /**
     * 上传mp4文件
     *
     * @param prefix      文件前缀
     * @param filename    文件名
     * @param inputStream 文件流
     * @return 文件全路径
     */
    public String uploadMp4File(String prefix, String filename, InputStream inputStream) {
        String filePath = buildFilePath(prefix, filename);
        try {
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .object(filePath)
                    .contentType("video/mp4")
                    .bucket(minioConfigurationProperties.getBucket()).stream(inputStream, inputStream.available(), -1)
                    .build();
            minioClient.putObject(putObjectArgs);
            return minioConfigurationProperties.getEndpoint() + separator + minioConfigurationProperties.getBucket() + separator + filePath;
        } catch (Exception e) {
            log.error("Minio put file error.", e);
            throw new RuntimeException("Minio put file error.");
        }
    }

    /**
     * 上传文件，由 MinIO 自动推断文件类型
     *
     * @param prefix      文件前缀
     * @param filename    文件名
     * @param inputStream 文件流
     * @return 文件全路径
     */
    public String uploadFile(String prefix, String filename, InputStream inputStream) {
        String filePath = buildFilePath(prefix, filename);
        try {
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .object(filePath)
                    .bucket(minioConfigurationProperties.getBucket()).stream(inputStream, inputStream.available(), -1)
                    .build();
            minioClient.putObject(putObjectArgs);
            return minioConfigurationProperties.getEndpoint() + separator + minioConfigurationProperties.getBucket() + separator + filePath;
        } catch (Exception e) {
            log.error("Minio put file error.", e);
            throw new RuntimeException("Minio put file error.");
        }
    }

    /**
     * 删除文件
     *
     * @param pathUrl 文件全路径
     */
    public void deleteFile(String pathUrl) {
        String key = pathUrl.replace(minioConfigurationProperties.getEndpoint() + separator, "");
        int index = key.indexOf(separator);
        String bucket = key.substring(0, index);
        String filePath = key.substring(index + 1);

        RemoveObjectArgs removeObjectArgs = RemoveObjectArgs.builder()
                .bucket(bucket)
                .object(filePath)
                .build();

        try {
            minioClient.removeObject(removeObjectArgs);
        } catch (Exception e) {
            log.error("minio remove file error.  pathUrl:{}", pathUrl);
            throw new RuntimeException("Minio remove file error.  pathUrl:" + pathUrl);
        }
    }


    /**
     * 下载文件
     *
     * @param pathUrl 文件全路径
     * @return 字节数组
     */
    public byte[] downloadFile(String pathUrl) {
        String key = pathUrl.replace(minioConfigurationProperties.getEndpoint() + separator, "");
        int index = key.indexOf(separator);
        String filePath = key.substring(index + 1);
        InputStream inputStream;

        try {
            GetObjectArgs getObjectArgs = GetObjectArgs.builder()
                    .bucket(minioConfigurationProperties.getBucket())
                    .object(filePath)
                    .build();
            inputStream = minioClient.getObject(getObjectArgs);
        } catch (Exception e) {
            log.error("Minio down file error.  pathUrl:{}", pathUrl);
            throw new RuntimeException("Minio down file error.  pathUrl:" + pathUrl);
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int returnCode = 0;
        while (true) {
            try {
                if (inputStream != null && !((returnCode = inputStream.read(buffer, 0, 1024)) > 0)) {
                    break;
                }
            } catch (IOException e) {
                throw new RuntimeException("Minio transfer file to byte array failed.");
            }
            byteArrayOutputStream.write(buffer, 0, returnCode);
        }

        return byteArrayOutputStream.toByteArray();
    }

}