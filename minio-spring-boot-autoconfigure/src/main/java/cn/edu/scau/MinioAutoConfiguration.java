package cn.edu.scau;

import io.minio.MinioClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(MinioConfigurationProperties.class)
@ConditionalOnProperty(prefix = "minio", name = {
        "endpoint",
        "accessKey",
        "secretKey",
        "bucket"
})
public class MinioAutoConfiguration {

    private final MinioConfigurationProperties minioConfigurationProperties;

    public MinioAutoConfiguration(MinioConfigurationProperties minioConfigurationProperties) {
        this.minioConfigurationProperties = minioConfigurationProperties;
    }

    @Bean
    public MinioClient minioClient() {
        return MinioClient
                .builder()
                .credentials(minioConfigurationProperties.getAccessKey(), minioConfigurationProperties.getSecretKey())
                .endpoint(minioConfigurationProperties.getEndpoint())
                .build();
    }

    @Bean
    public MinioFileStorageUtil minioFileStorageService(MinioClient minioClient, MinioConfigurationProperties minioConfigurationProperties) {
        return new MinioFileStorageUtil(minioClient, minioConfigurationProperties);
    }

}